### What is this repository for? ###

* A framework for a generic command-side pipeline meant to allow you to focus on the business logic and forget about the other CRUD. 

### How do I get set up? ###

* Uses dotnet core 2

### Contribution guidelines ###

To come.
Things that would be cool though are:

- An attribute that identifies a property as being associated with a certain fact (e.g. the `Deleted` property should produce a `Deleted<TFlatModel>` fact, even if it came through as part of an update command)
- A generic FK validation checker (even though I hate them)
- Switch Created fact from using the actual class to using a Dictionary instead . . . probably. That way we avoid sadness trying to replay older versions of facts (after a property is removed, added, etc.)
- etc.