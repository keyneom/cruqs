﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace cruqs
{
    public interface ICommandMediator
    {
        Task<Dictionary<Guid, ICommandResult>> Execute(IEnumerable<ICommand> commands, bool continueOnFailure = false);
    }

    public class CommandMediator : ICommandMediator
    {
        protected IAggregateInstanceProvider _aggregateInstanceProvider;
        protected IFactRepository _factRepository;

        public CommandMediator(IAggregateInstanceProvider aggregateInstanceProvider, IFactRepository factRepository)
        {
            _aggregateInstanceProvider = aggregateInstanceProvider;
            _factRepository = factRepository;
        }

        public async Task<Dictionary<Guid, ICommandResult>> Execute(IEnumerable<ICommand> commands, bool continueOnFailure = false)
        {
            var aggregateRootIds = commands.Select(c => c.AggregateRootId).Distinct().ToList();
            await _aggregateInstanceProvider.LockAggregateRoots(aggregateRootIds);
            await _aggregateInstanceProvider.LoadAggregateRootFacts(aggregateRootIds);
            ICommand currentCommand = null;
            var commandResults = new Dictionary<Guid, ICommandResult>();
            var errorEncountered = false;
            try
            {
                foreach (var command in commands)
                {
                    currentCommand = command;
                    var aggregateRoot = await _aggregateInstanceProvider.GetAggregateRoot(aggregateRootId: command.AggregateRootId, flatModelType: new Lazy<Type>(() =>
                    {
                        return command.GetType().GetInterfaces().Where(t => t.IsGenericType).FirstOrDefault(t => t.GetGenericTypeDefinition() == typeof(ICommand<>))?.GetGenericArguments()[0];
                    }));

                    var commandResult = await aggregateRoot.Handle(command);
                    commandResults[command.CommandId] = commandResult;

                    // If this command is not being persisted we roll back the InMemoryState immediately
                    if (!command.ShouldPersist)
                    {
                        await aggregateRoot.UndoFacts(commandResult.FactsGenerated);
                        // We don't care if commands that are not persisting fail, we'll keep processing until we encounter a legit failure
                        continue;
                    }

                    // If we have encountered an error and the continueOnFailure flag is not set then break and stop processing commands
                    if (commandResult.Status == ResultStatus.Failure && !continueOnFailure)
                    {
                        errorEncountered = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                errorEncountered = true;
                commandResults[currentCommand.CommandId] = new CommandResult(currentCommand.CommandId, errors: new List<string> { ex.ToString() });
            }
            finally
            {
                if (!errorEncountered)
                {
                    var factsToPersist = _aggregateInstanceProvider.GetInstantiatedAggregateRoots().SelectMany(ar =>
                    {
                        return ar.UnprocessedFacts;
                    });
                    try
                    {
                        // Should we just have the repository publish the facts as well?
                        await _factRepository.Persist(factsToPersist);
                    }
                    catch (Exception)
                    {
                        foreach (var ar in _aggregateInstanceProvider.GetInstantiatedAggregateRoots())
                        {
                            await ar.UndoFacts(ar.UnprocessedFacts);
                        }
                        foreach (var commandResult in commandResults)
                        {
                            commandResult.Value.AddError("Failed persisting to DB");
                        }
                    }

                    foreach (var ar in _aggregateInstanceProvider.GetInstantiatedAggregateRoots())
                    {
                        ar.PersistedFacts.AddRange(ar.UnprocessedFacts);
                        ar.UnprocessedFacts.Clear();

                        await ar.ApplyFacts(ar.PersistedFacts, toPersistedState: true);
                    }
                }
                else
                {
                    foreach (var ar in _aggregateInstanceProvider.GetInstantiatedAggregateRoots())
                    {
                        await ar.UndoFacts(ar.UnprocessedFacts);
                    }
                }
            }
            return commandResults;
        }
    }
}
