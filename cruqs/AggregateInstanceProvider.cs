﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace cruqs
{
    public interface IAggregateInstanceProvider
    {
        IAggregate GetAggregate(Lazy<Type> flatModelType, Guid entityId);
        IAggregate<TFlatModel> GetAggregate<TFlatModel>(Guid entityId) where TFlatModel : IFlatModel;
        Task<IAggregateRoot> GetAggregateRoot(Lazy<Type> flatModelType, Guid aggregateRootId);
        Task<IAggregateRoot<TFlatModel>> GetAggregateRoot<TFlatModel>(Guid aggregateRootId) where TFlatModel : IFlatModel;
        IEnumerable<IAggregateRoot> GetInstantiatedAggregateRoots();

        Task LockAggregateRoots(IEnumerable<Guid> aggregateRootIds);
        Task LoadAggregateRootFacts(IEnumerable<Guid> aggregateRootIds);
        Task UnLockAggregateRoots(IEnumerable<Guid> aggregateRootIds);
    }

    public class AggregateInstanceProvider : IAggregateInstanceProvider
    {
        IDependencyContainer _container { get; set; }
        IFactRepository _factRepository { get; set; }
        IWriteLocker _writeLocker { get; set; }

        Dictionary<Guid, IAggregateRoot> AggregateRoots { get; set; }
        Dictionary<Guid, IAggregate> NestedAggregates { get; set; }
        IDictionary<Guid, IReadOnlyCollection<IPersistedFact>> Facts { get; set; }

        public int MAX_RETRIES = 7;
        public string WriteLockInstanceId = Guid.NewGuid().ToString();

        public AggregateInstanceProvider(IDependencyContainer container, IFactRepository factRepository, IWriteLocker writeLocker)
        {
            _container = container;
            _factRepository = factRepository;
            _writeLocker = writeLocker;

            AggregateRoots = new Dictionary<Guid, IAggregateRoot>();
            NestedAggregates = new Dictionary<Guid, IAggregate>();
        }

        public async Task<IAggregateRoot> GetAggregateRoot(Lazy<Type> flatModelType, Guid aggregateRootId)
        {
            var alreadyLoaded = AggregateRoots.TryGetValue(aggregateRootId, out var aggregateRoot);
            if (!alreadyLoaded)
            {
                var neededType = typeof(IAggregateRoot<>).MakeGenericType(flatModelType.Value);
                aggregateRoot = await InstantiateAggregateRoot(aggregateRootId, neededType);
            }
            return aggregateRoot;
        }

        public IAggregate GetAggregate(Lazy<Type> flatModelType, Guid entityId)
        {
            IAggregate typedAggregate = null;

            // We may already have loaded the requested aggregate as an aggregate root, so we'll check there first
            var isAnAggregateRoot = AggregateRoots.TryGetValue(entityId, out var aggregateRoot);
            if (isAnAggregateRoot)
            {
                typedAggregate = aggregateRoot;
            }
            else
            {
                // We may have already been asked to load the nested aggregate
                var alreadyLoaded = NestedAggregates.TryGetValue(entityId, out var aggregate);
                if (alreadyLoaded)
                {
                    typedAggregate = aggregate;
                }
                else
                {
                    // If we don't have the aggregate in either of our caches, it is because an aggregate root is in the process of building itself out
                    // we'll new up an instance of the requested Nested Aggregate type for the AggregateRoot to work with
                    typedAggregate = _container.GetInstance(typeof(IAggregate<>).MakeGenericType(flatModelType.Value)) as IAggregate;
                    NestedAggregates[entityId] = typedAggregate;
                }
            }
            return typedAggregate;
        }

        public IAggregate<TFlatModel> GetAggregate<TFlatModel>(Guid entityId) where TFlatModel : IFlatModel
        {
            return GetAggregate(new Lazy<Type>(() => typeof(TFlatModel)), entityId) as IAggregate<TFlatModel>;
        }

        public async Task<IAggregateRoot<TFlatModel>> GetAggregateRoot<TFlatModel>(Guid aggregateRootId) where TFlatModel : IFlatModel
        {
            var alreadyLoaded = AggregateRoots.TryGetValue(aggregateRootId, out var aggregateRoot);
            if (!alreadyLoaded)
            {
                aggregateRoot = await InstantiateAggregateRoot(aggregateRootId, typeof(IAggregateRoot<TFlatModel>));
            }
            return aggregateRoot as IAggregateRoot<TFlatModel>;
        }

        public async Task LoadAggregateRootFacts(IEnumerable<Guid> aggregateRootIds)
        {
            var factResults = await _factRepository.GetFactsByAggregateRootIds(aggregateRootIds);
            if (Facts == null)
            {
                Facts = factResults;
            }
            else
            {
                foreach (var aggInfo in factResults)
                {
                    Facts[aggInfo.Key] = aggInfo.Value;
                }
            }
        }

        public Task UnLockAggregateRoots(IEnumerable<Guid> aggregateRootIds)
        {
            foreach (var aggregateRootId in aggregateRootIds)
            {
                AggregateRoots.Remove(aggregateRootId);
            }
            return _writeLocker.Release(aggregateRootIds.Select(aggId => aggId.ToString()));
        }

        // It would be nice to modify this to allow work to proceed as soon as the lock is resolved for the given aggregate
        public Task LockAggregateRoots(IEnumerable<Guid> aggregateRootIds)
        {
            return _writeLocker.Lock(aggregateRootIds.Select(aggId => aggId.ToString()), WriteLockInstanceId, new TimeSpan(0, 5, 0));
            // // Really we should be calling the Lock overload that takes an IEnumerable of strings, but this gives an example implementation in the case that the underlying lock management can't handle multiple entries at once
            // var tasks = aggregateRootIds.AsParallel().Select(async aggregateRootId =>
            // {
            //     var attainedLock = false;
            //     for (var attempt = 0; attempt < MAX_RETRIES; attempt++)
            //     {
            //         await Task.Delay(TimeSpan.FromMilliseconds(GetMillisecondsToDelay(attempt))).ConfigureAwait(false);
            //         if (attainedLock = await _writeLocker.Lock(aggregateRootId.ToString(), WriteLockInstanceId, new TimeSpan(0, 5, 0)))
            //         {
            //             break;
            //         }
            //     }
            //     if (!attainedLock)
            //     {
            //         throw new Exception($"Failed to secure a lock for AggregateRootId: '{aggregateRootId}'");
            //     }
            // });

            // return Task.WhenAll(tasks);
        }

        private async Task<IAggregateRoot> InstantiateAggregateRoot(Guid aggregateRootId, Type type)
        {
            var aggregateRoot = _container.GetInstance(type) as IAggregateRoot;
            AggregateRoots[aggregateRootId] = aggregateRoot;
            var persistedFacts = Facts[aggregateRootId];
            var facts = persistedFacts.Select(persistedFact => persistedFact.Fact).ToList();
            aggregateRoot.PersistedFacts.AddRange(facts);
            await aggregateRoot.ApplyFacts(facts, toPersistedState: true);
            return aggregateRoot;
        }

        // Exponential Back-off, MAX_ATTEMPTS = 7 means something on the order of waiting a combined total of 42 secs
        private double GetMillisecondsToDelay(int attempt)
        {
            return Math.Pow(attempt, 2) * 300;
        }

        public IEnumerable<IAggregateRoot> GetInstantiatedAggregateRoots()
        {
            return AggregateRoots.Values;
        }
    }
}
