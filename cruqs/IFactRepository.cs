using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace cruqs
{
    public interface IFactRepository
    {
        Task Persist(IEnumerable<IFact> facts);
        Task<IPersistedFact> GetFactById(Guid factId);
        Task<IQueryable<IPersistedFact>> GetAllAsync();
        Task<IQueryable<IPersistedFact>> GetAllSinceTimeStampAsync(DateTimeOffset dateTimeOffset);
        Task<IQueryable<IPersistedFact>> GetAllFactsForAggregate(Guid aggregateId);
        Task<IDictionary<Guid,IReadOnlyCollection<IPersistedFact>>> GetFactsByAggregateRootIds(IEnumerable<Guid> aggregateIds);
    }
}