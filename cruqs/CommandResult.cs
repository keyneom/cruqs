﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace cruqs
{
    public enum ResultStatus
    {
        Success = 1,
        Warning = 2,
        Failure = 3
    }

    public interface ICommandResult
    {
        Guid CommandId { get; }
        ResultStatus Status { get; }
        IEnumerable<IFact> FactsGenerated { get; }
        List<string> Warnings { get; }
        void AddWarning(string warning);
        void AddWarnings(IEnumerable<string> warnings);
        List<string> Errors { get; }
        void AddError(string error);
        void AddErrors(IEnumerable<string> errors);
    }

    public class CommandResult : ICommandResult
    {
        public Guid CommandId { get; set; }
        public ResultStatus Status { get; set; }
        public IEnumerable<IFact> FactsGenerated { get; set; }
        public List<string> Warnings { get; set; }
        public List<string> Errors { get; set; }

        public CommandResult(Guid commandId, IEnumerable<string> warnings = null, IEnumerable<string> errors = null)
        {
            CommandId = commandId;
            Warnings = warnings?.ToList() ?? new List<string>();
            Errors = errors?.ToList() ?? new List<string>();
            Status = Errors.Any() ? ResultStatus.Failure : Warnings.Any() ? ResultStatus.Warning : ResultStatus.Success;
        }

        public void AddWarning(string warning)
        {
            Warnings.Add(warning);
            Status = Status == ResultStatus.Success ? ResultStatus.Warning : Status;
        }

        public void AddWarnings(IEnumerable<string> warnings)
        {
            Warnings.AddRange(warnings);
            Status = Status == ResultStatus.Success ? ResultStatus.Warning : Status;
        }
        
        public void AddError(string error)
        {
            Errors.Add(error);
            Status = ResultStatus.Failure;
        }

        public void AddErrors(IEnumerable<string> errors)
        {
            Errors.AddRange(errors);
            Status = ResultStatus.Failure;
        }
    }
}
