using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel;

namespace cruqs.helpers
{
    using PropertyName = System.String;
    using PropertyValue = System.String;

    public static class PropertyInfoExtensions
    {
        // Lazyness and ConcurrentDictionaries are used to avoid concurrency issues, we migth be able to remove laziness for a performance increase
        private static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> _propInfoCache = new Dictionary<Type, Dictionary<string, PropertyInfo>>(31);
        public static PropertyInfo GetCachedProperty(this Type type, string propertyName)
        {
            // Get our cached properties for the given type, if we haven't cached anything yet for the given type yet, then create a new dict for that type
            try
            {
                return _propInfoCache[type][propertyName];
            }
            catch (Exception)
            {
                var propInfo = _propInfoCache[type] = type.GetProperties().ToDictionary((p) => p.Name, (p) => p);
                return propInfo[propertyName];
            }
        }

        public static Dictionary<string, PropertyInfo> GetCachedProperties(this Type type)
        {
            // Get our cached properties for the given type, if we haven't cached anything yet for the given type yet, then create a new dict for that type
            try
            {
                return _propInfoCache[type];
            }
            catch (Exception)
            {
                var propInfos = _propInfoCache[type] = type.GetProperties().ToDictionary((p) => p.Name, (p) => p);
                return propInfos;
            }
        }

        private static readonly Dictionary<PropertyInfo, Delegate> _propGetterCache = new Dictionary<PropertyInfo, Delegate>(31);
        public static Func<T, object> PropertyGetter<T>(this PropertyInfo propertyInfo)
        {
            try
            {
                return (Func<T, object>)_propGetterCache[propertyInfo];
            }
            catch (Exception)
            {
                var getterDelegate = _propGetterCache[propertyInfo] = Delegate.CreateDelegate(typeof(Func<T, object>), propertyInfo.GetGetMethod(nonPublic: true));
                return (Func<T, object>)getterDelegate;
            }
        }

        private static readonly Dictionary<PropertyInfo, Delegate> _propSetterCache = new Dictionary<PropertyInfo, Delegate>(31);
        public static Action<T, object> PropertySetter<T>(this PropertyInfo propertyInfo)
        {
            try
            {
                return (Action<T, object>)_propSetterCache[propertyInfo];
            }
            catch (Exception)
            {
                var instanceExpression = Expression.Parameter(propertyInfo.DeclaringType, "i");
                var argument = Expression.Parameter(typeof(object), "a");
                var setterCall = Expression.Call(instanceExpression, propertyInfo.GetSetMethod(), Expression.Convert(argument, propertyInfo.PropertyType));
                var setterDelegate = _propSetterCache[propertyInfo] = Expression.Lambda(setterCall, instanceExpression, argument).Compile();
                return (Action<T, object>)setterDelegate;
            }
        }

        private static readonly Dictionary<Type, Delegate> _updaterCache = new Dictionary<Type, Delegate>();
        public static void Update<T>(this Type type, T instance, Dictionary<PropertyName, PropertyValue> updates)
        {
            if (!_updaterCache.TryGetValue(type, out var updaterDelegate))
            {
                var updatesType = typeof(Dictionary<PropertyName, PropertyValue>);
                var instanceArg = Expression.Parameter(type, "i");
                var updatesArg = Expression.Parameter(updatesType, "u");
                var containsKeyMethod = updatesType.GetMethod("ContainsKey");
                var setterExpressions = new List<Expression>();
                foreach (var propInfoKeyValuePair in type.GetCachedProperties())
                {
                    var propertyName = propInfoKeyValuePair.Key;
                    var propertyInfo = propInfoKeyValuePair.Value;
                    // u.ContainsKey(propertyName)
                    var containsKeyExpression = Expression.Call(updatesArg, containsKeyMethod, Expression.Constant(propertyName));
                    // u.Item[propertyName]
                    var keyExpression = Expression.Property(updatesArg, "Item", new Expression[] { Expression.Constant(propertyName) });
                    var typeConverter = TypeDescriptor.GetConverter(propertyInfo.PropertyType);
                    var typeConverterVariable = Expression.Constant(typeConverter);
                    var converterMethod = TypeDescriptor.GetConverter(propertyInfo.PropertyType).GetType().GetMethod("ConvertFromString", new[] { typeof(string) });
                    // var convertedValue = TypeDescriptor.GetConverter(propertyInfo.PropertyType).ConvertFromString(u.Item[propertyName])
                    var convertedValExpression = Expression.Call(typeConverterVariable, converterMethod, keyExpression);
                    // i.PropertyName
                    var propertyExpression = Expression.Property(instanceArg, propertyInfo);
                    // u.ContainsKey(propertyName) ? convertedValue : i.PropertyName
                    var ternaryExpression = Expression.Condition(containsKeyExpression, Expression.Convert(convertedValExpression, propertyInfo.PropertyType), propertyExpression);
                    var setPropertyIfInUpdatesCall = Expression.Call(instanceArg, propertyInfo.GetSetMethod(), ternaryExpression);
                    setterExpressions.Add(setPropertyIfInUpdatesCall);
                }
                var updateFromDictExpression = Expression.Block(setterExpressions);
                updaterDelegate = _updaterCache[type] = Expression.Lambda(updateFromDictExpression, instanceArg, updatesArg).Compile();
            }
            ((Action<T, Dictionary<PropertyName, PropertyValue>>)updaterDelegate)(instance, updates);
        }

        private static readonly Dictionary<Type, Delegate> _creatorCache = new Dictionary<Type, Delegate>();
        public static T Create<T>(this Type type, Dictionary<PropertyName, PropertyValue> instanceValues)
        {
            if (!_creatorCache.TryGetValue(type, out var creatorDelegate))
            {
                var instanceValuesType = typeof(Dictionary<PropertyName, PropertyValue>);
                var instanceValuesArg = Expression.Parameter(instanceValuesType, "iv");
                var containsKeyMethod = instanceValuesType.GetMethod("ContainsKey");
                var instantiateTypeExpression = Expression.New(typeof(T));
                var returnValueExpression = Expression.Variable(typeof(T), "flatModelInstance");
                var expressions = new List<Expression>()
                {
                    Expression.Assign(returnValueExpression, instantiateTypeExpression),
                };

                foreach (var propInfoKeyValuePair in type.GetCachedProperties())
                {
                    var propertyName = propInfoKeyValuePair.Key;
                    var propertyInfo = propInfoKeyValuePair.Value;
                    // u.ContainsKey(propertyName)
                    var containsKeyExpression = Expression.Call(instanceValuesArg, containsKeyMethod, Expression.Constant(propertyName));
                    // u.Item[propertyName]
                    var keyExpression = Expression.Property(instanceValuesArg, "Item", new Expression[] { Expression.Constant(propertyName) });
                    var typeConverter = TypeDescriptor.GetConverter(propertyInfo.PropertyType);
                    var typeConverterVariable = Expression.Constant(typeConverter);
                    var converterMethod = TypeDescriptor.GetConverter(propertyInfo.PropertyType).GetType().GetMethod("ConvertFromString", new[] { typeof(string) });
                    // var convertedValue = TypeDescriptor.GetConverter(propertyInfo.PropertyType).ConvertFromString(u.Item[propertyName])
                    var convertedValExpression = Expression.Call(typeConverterVariable, converterMethod, keyExpression);
                    // i.PropertyName
                    var propertyExpression = Expression.Property(returnValueExpression, propertyInfo);
                    // u.ContainsKey(propertyName) ? convertedValue : i.PropertyName
                    var ternaryExpression = Expression.Condition(containsKeyExpression, Expression.Convert(convertedValExpression, propertyInfo.PropertyType), propertyExpression);
                    var setPropertyIfInUpdatesCall = Expression.Call(returnValueExpression, propertyInfo.GetSetMethod(), ternaryExpression);
                    expressions.Add(setPropertyIfInUpdatesCall);
                }

                expressions.Add(returnValueExpression);

                var createFromDictExpression = Expression.Block(new[] { returnValueExpression }, expressions);
                creatorDelegate = _creatorCache[type] = Expression.Lambda(createFromDictExpression, instanceValuesArg).Compile();
            }
            return ((Func<Dictionary<PropertyName, PropertyValue>, T>)creatorDelegate)(instanceValues);
        }
    }
}