using System;
using System.Threading.Tasks;

namespace cruqs.helpers
{
    public static class StringExtension
    {
        public static Guid ToGuid(this string source)
        {
            return string.IsNullOrWhiteSpace(source) ? Guid.Empty : Guid.Parse(source);
        }

        public static Guid? ToNullableGuid(this string source)
        {
            return string.IsNullOrWhiteSpace(source) ? null : (Guid?) Guid.Parse(source);
        }
    }
}