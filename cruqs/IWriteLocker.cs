using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace cruqs
{
    public interface IWriteLocker
    {
        // Locking can usually be implemented using a Cache with an `AddOrGet` method
        Task<bool> Lock(string aggregateId, string lockingInstanceId, TimeSpan? optionalExpiration = null);
        Task<bool> Lock(IEnumerable<string> aggregateId, string lockingInstanceId, TimeSpan? optionalExpiration = null);
        Task<bool> Release(string aggregateId);
        Task<bool> Release(IEnumerable<string> aggregateId);
    }
}