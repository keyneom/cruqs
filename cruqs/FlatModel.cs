﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace cruqs
{
    public interface IFlatModel
    {
        Guid Id { get; set; }
        bool Deleted { get; set; }
        IFlatModel Clone();
    }

    public class FlatModel : IFlatModel
    {
        public Guid Id { get; set; }
        public bool Deleted { get; set; }

        public IFlatModel Clone()
        {
            return (IFlatModel) MemberwiseClone();
        }
    }
}
