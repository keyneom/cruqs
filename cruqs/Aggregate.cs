﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using cruqs.helpers;
using System.Linq;

namespace cruqs
{
    using PropertyName = System.String;
    using PropertyValue = System.String;

    public interface IAggregate
    {
        Task<ICommandResult> Validate(ICommandResult commandResult);

        Task AttachToParent(bool toPersistedState);
        void ResetNestedAggregates();
        Task AddNestedAggregate<TNestedAggregate, TNestedFlatModel>(TNestedAggregate nestedAggregate, bool toPersistedState) where TNestedAggregate : Aggregate<TNestedFlatModel> where TNestedFlatModel : IFlatModel;

        Task<IEnumerable<IFact>> GetFacts(ICommand command);
        Task ApplyFact(IFact fact, bool toPersistedState);
        Task ApplyFacts(IEnumerable<IFact> facts, bool toPersistedState);
    }

    public interface IAggregate<TFlatModel> : IAggregate where TFlatModel : IFlatModel
    {
        TFlatModel InMemoryState { get; }
        TFlatModel PersistedState { get; }
    }

    public abstract class Aggregate<TFlatModel> : IAggregate<TFlatModel> where TFlatModel : IFlatModel
    {
        public TFlatModel InMemoryState { get; set; }
        public TFlatModel PersistedState { get; set; }

        public HashSet<Type> HasOne = new HashSet<Type>();
        public Dictionary<Type, IAggregate> HasOneNestedAggregates = new Dictionary<Type, IAggregate>();
        public Dictionary<Type, IAggregate> HasOnePersistedNestedAggregates = new Dictionary<Type, IAggregate>();
        public HashSet<Type> HasMany = new HashSet<Type>();
        public Dictionary<Type, Dictionary<Guid, IAggregate>> HasManyNestedAggregates = new Dictionary<Type, Dictionary<Guid, IAggregate>>();
        public Dictionary<Type, Dictionary<Guid, IAggregate>> HasManyPersistedNestedAggregates = new Dictionary<Type, Dictionary<Guid, IAggregate>>();

        public virtual Task AddNestedAggregate<TNestedAggregate, TNestedFlatModel>(TNestedAggregate nestedAggregate, bool toPersistedState) where TNestedAggregate : Aggregate<TNestedFlatModel> where TNestedFlatModel : IFlatModel
        {
            if (HasOne.Contains(typeof(TNestedFlatModel)))
            {
                HasOneNestedAggregates[typeof(TNestedFlatModel)] = nestedAggregate;
                if (toPersistedState) HasOnePersistedNestedAggregates[typeof(TNestedFlatModel)] = nestedAggregate;
            }
            else if (HasMany.Contains(typeof(TNestedFlatModel)))
            {
                if (!HasManyNestedAggregates.TryGetValue(typeof(TNestedFlatModel), out var existingNestedAggregates))
                {
                    HasManyNestedAggregates[typeof(TNestedFlatModel)] = existingNestedAggregates = new Dictionary<Guid, IAggregate>();
                }
                existingNestedAggregates[nestedAggregate.InMemoryState.Id] = nestedAggregate;

                if (toPersistedState)
                {
                    if (!HasManyPersistedNestedAggregates.TryGetValue(typeof(TNestedFlatModel), out var existingPersistedNestedAggregates)) HasManyPersistedNestedAggregates[typeof(TNestedFlatModel)] = existingPersistedNestedAggregates = new Dictionary<Guid, IAggregate>();
                    existingPersistedNestedAggregates[nestedAggregate.InMemoryState.Id] = nestedAggregate;
                }
            }
            else
            {
                throw new NotImplementedException($"The NestedAggregate for the FlatModel: {typeof(TNestedFlatModel)} is neither in the HasOne nor in the HasMany sets. You probably want to add it to one of the two in order to get things working. If neither of those fit the situation well then you'll want to override and probably call this base method after and special casing.");
            }
            return Task.CompletedTask;
        }

        public virtual void ResetNestedAggregates()
        {
            foreach (var nestedAggregate in HasOneNestedAggregates.Values)
            {
                nestedAggregate.ResetNestedAggregates();
            }
            HasOneNestedAggregates.Clear();

            foreach (var nestedAggregateDictionary in HasManyNestedAggregates.Values)
            {
                foreach (var nestedAggregate in nestedAggregateDictionary.Values)
                {
                    nestedAggregate.ResetNestedAggregates();
                }
                nestedAggregateDictionary.Clear();
            }
            HasManyNestedAggregates.Clear();
        }

        public virtual async Task ApplyFacts(IEnumerable<IFact> facts, bool toPersistedState)
        {
            foreach (var fact in facts)
            {
                await ApplyFact(fact, toPersistedState: toPersistedState);
            }
        }

        public virtual Task ApplyFact(IFact fact, bool toPersistedState)
        {
            switch (fact)
            {
                case Created<TFlatModel> created:
                    InMemoryState = typeof(TFlatModel).Create<TFlatModel>(created.Entity);
                    PersistedState = toPersistedState ? typeof(TFlatModel).Create<TFlatModel>(created.Entity) : PersistedState;
                    break;
                case Updated<TFlatModel> updated:
                    typeof(TFlatModel).Update(InMemoryState, updated.Updates);
                    if (toPersistedState) typeof(TFlatModel).Update(PersistedState, updated.Updates);
                    break;
                case Deleted<TFlatModel> deleted:
                    InMemoryState.Deleted = true;
                    if (PersistedState != null) PersistedState.Deleted = toPersistedState ? true : PersistedState.Deleted;
                    break;
                default:
                    throw new NotImplementedException($"Unknown fact type: {fact.GetType()}. You will need to override to add support!");
            }
            return Task.CompletedTask;
        }

        public abstract Task AttachToParent(bool toPersistedState);

        public virtual Task<IEnumerable<IFact>> GetFacts(ICommand command)
        {
            var facts = new List<IFact>();

            switch (command)
            {
                case Create<TFlatModel> create:
                    facts.Add(new Created<TFlatModel>(create.AggregateRootId, create.Entity));
                    break;
                case Update<TFlatModel> update:
                    facts.Add(new Updated<TFlatModel>(update.AggregateRootId, update.EntityId, update.Updates));
                    break;
                case Delete<TFlatModel> delete:
                    facts.Add(new Deleted<TFlatModel>(delete.AggregateRootId, delete.EntityId));
                    break;
                default:
                    throw new NotImplementedException($"Unknown command type: {command.GetType()}. You will need to override to add support!");
            }

            return Task.FromResult(facts.AsEnumerable());
        }

        public virtual async Task<ICommandResult> Validate(ICommandResult commandResult)
        {
            var entityId = InMemoryState?.Id;
            if (entityId == null) commandResult.AddError("Aggregate is null.");
            if (entityId == Guid.Empty) commandResult.AddError("EntityId is Empty, for safety adding an error.");

            // Each Aggregate ensures that all of it's children are validated as well
            foreach (var nestedAggregate in HasOneNestedAggregates.Values)
            {
                await nestedAggregate.Validate(commandResult);
            }

            foreach (var nestedAggregateGrouping in HasManyNestedAggregates.Values)
            {
                foreach (var nestedAggregate in nestedAggregateGrouping.Values)
                {
                    await nestedAggregate.Validate(commandResult);
                }
            }

            return commandResult;
        }
    }
}
