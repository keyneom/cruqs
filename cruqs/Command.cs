﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace cruqs
{
    using PropertyName = System.String;
    using PropertyValue = System.String;

    public interface ICommand
    {
        Guid CommandId { get; }
        Guid AggregateRootId { get; }
        Guid EntityId { get; }
        bool ShouldPersist { get; }
    }

    public interface ICommand<TFlatModel> : ICommand where TFlatModel : IFlatModel { }

    public abstract class Command<TFlatModel> : ICommand<TFlatModel> where TFlatModel : IFlatModel
    {
        public Guid CommandId { get; set; }
        public Guid AggregateRootId { get; set; }
        public Guid EntityId { get; set; }
        public bool ShouldPersist { get; set; } = true;
    }

    public class Create<TFlatModel> : Command<TFlatModel> where TFlatModel : IFlatModel
    {
        public Dictionary<PropertyName, PropertyValue> Entity { get; set; }
    }

    public class Update<TFlatModel> : Command<TFlatModel> where TFlatModel : IFlatModel
    {
        public Dictionary<PropertyName, PropertyValue> Updates { get; set; }
    }

    public class Delete<TFlatModel> : Command<TFlatModel> where TFlatModel : IFlatModel { }
}
