﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace cruqs
{
    public interface IDependencyContainer
    {
        T GetInstance<T>();
        object GetInstance(Type type);
    }
}
