﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reflection;

namespace cruqs
{
    public interface IAggregateRoot : IAggregate
    {
        List<IFact> PersistedFacts { get; set; }
        List<IFact> UnprocessedFacts { get; set; }

        Task<ICommandResult> Handle(ICommand command);
        Task UndoFacts(IEnumerable<IFact> factsToUndo);
    }

    public interface IAggregateRoot<TFlatModel> : IAggregateRoot where TFlatModel : IFlatModel { }

    public abstract class AggregateRoot<TFlatModel> : Aggregate<TFlatModel>, IAggregateRoot<TFlatModel> where TFlatModel : IFlatModel
    {
        private readonly IAggregateInstanceProvider _aggregateInstanceProvider;

        public List<IFact> PersistedFacts { get; set; }
        public List<IFact> UnprocessedFacts { get; set; }

        protected AggregateRoot(IAggregateInstanceProvider aggregateInstanceProvider)
        {
            PersistedFacts = new List<IFact>();
            UnprocessedFacts = new List<IFact>();

            _aggregateInstanceProvider = aggregateInstanceProvider;
        }

        public override Task AttachToParent(bool toPersistedState)
        {
            // Since we are the RootAggregate we don't need to attach to a parent
            return Task.CompletedTask;
        }

        public async Task<ICommandResult> Handle(ICommand command)
        {
            var facts = await GetFacts(command);
            return await ProcessFacts(command.CommandId, facts);
        }

        public Task UndoFacts(IEnumerable<IFact> factsToUndo)
        {
            // This must opperate against all nestes aggregates as well, so we need to replay everything
            UnprocessedFacts = UnprocessedFacts.Except(factsToUndo).ToList();
            var remainingFacts = PersistedFacts.Union(UnprocessedFacts);
            // This is probably the only time this needs to be called
            ResetNestedAggregates();
            if (!remainingFacts.Any()) InMemoryState = default(TFlatModel);
            return ApplyFacts(remainingFacts, toPersistedState: false);
        }

        public async Task<ICommandResult> ProcessFacts(Guid commandId, IEnumerable<IFact> facts)
        {
            ICommandResult result = new CommandResult(commandId) { FactsGenerated = facts };
            UnprocessedFacts.AddRange(facts);
            await ApplyFacts(facts, toPersistedState: false);
            result = await Validate(result);

            return result;
        }

        public override async Task<IEnumerable<IFact>> GetFacts(ICommand command)
        {
            var rootCommand = command as ICommand<TFlatModel>;
            if (rootCommand != null)
            {
                return await base.GetFacts(command);
            }
            else
            {
                // If the command is for an entity that we can't handle directly then the EntityId should not be the same as the AggregateRootId
                if (command.EntityId == command.AggregateRootId) throw new Exception("The EntityId should not equal the AggregateId if the command is for a FlatModel other than the RootAggregate's FlatModel");

                var nestedAggregate = _aggregateInstanceProvider.GetAggregate(entityId: command.EntityId, flatModelType: new Lazy<Type>(() =>
                {
                    return command.GetType().GetInterface(typeof(ICommand<>).Name).GetGenericArguments()[0];
                }));
                return await nestedAggregate.GetFacts(command);
            }
        }

        public override async Task ApplyFact(IFact fact, bool toPersistedState)
        {
            var rootFact = fact as IFact<TFlatModel>;
            if (rootFact != null)
            {
                await base.ApplyFact(fact, toPersistedState);
            }
            else
            {
                // In the case that this is a fact for one of our nested aggregates, pass it on
                var nestedAggregate = _aggregateInstanceProvider.GetAggregate(entityId: fact.EntityId, flatModelType: new Lazy<Type>(() =>
                {
                    return fact.GetType().GetInterface(typeof(IFact<>).Name).GetGenericArguments()[0];
                }));
                await nestedAggregate.ApplyFact(fact, toPersistedState);
                await nestedAggregate.AttachToParent(toPersistedState);
            }
        }
    }
}
