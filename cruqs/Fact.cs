﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using cruqs.helpers;

namespace cruqs
{
    using PropertyName = System.String;
    using PropertyValue = System.String;

    public interface IFact
    {
        Guid AggregateRootId { get; }
        Guid EntityId { get; }
    }

    public interface IFact<TFlatModel> : IFact where TFlatModel : IFlatModel { }

    public interface IPersistedFact
    {
        Guid FactId { get; }
        Guid AggregateRootId { get; }
        string AggregateRootType { get; }
        Guid EntityId { get; }
        string EntityType { get; }
        DateTimeOffset CreatedAt { get; }
        Guid CreatedByUserId { get; }
        IFact Fact { get; }
    }

    public class Created<TFlatModel> : IFact<TFlatModel> where TFlatModel : IFlatModel
    {
        public Guid AggregateRootId { get; set; }
        public Guid EntityId { get; set; }
        public Dictionary<PropertyName, PropertyValue> Entity { get; set; }

        public Created() { }
        public Created(Guid aggregateRootId, Dictionary<PropertyName, PropertyValue> entity)
        {
            AggregateRootId = aggregateRootId;
            EntityId = entity["Id"].ToGuid();
            Entity = entity;
        }
    }

    public class Updated<TFlatModel> : IFact<TFlatModel> where TFlatModel : IFlatModel
    {
        public Guid AggregateRootId { get; set; }
        public Guid EntityId { get; set; }

        public Dictionary<PropertyName, PropertyValue> Updates { get; set; }

        public Updated() { }
        public Updated(Guid aggregateRootId, Guid entityId, Dictionary<PropertyName, PropertyValue> updates)
        {
            AggregateRootId = aggregateRootId;
            EntityId = entityId;
            Updates = updates;
        }
    }

    public class Deleted<TFlatModel> : IFact<TFlatModel> where TFlatModel : IFlatModel
    {
        public Guid AggregateRootId { get; set; }
        public Guid EntityId { get; set; }

        public Deleted() { }
        public Deleted(Guid aggregateRootId, Guid entityId)
        {
            AggregateRootId = aggregateRootId;
            EntityId = entityId;
        }
    }
}
