using System;
using Xunit;
using cruqs.helpers;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace cruqs.test
{
    public class TFlatModel : FlatModel
    {
        public string Data { get; set; }
    }

    public class TRootAggregate : AggregateRoot<TFlatModel>, IAggregateRoot<TNestedFlatModel>
    {
        public TRootAggregate(IAggregateInstanceProvider aggregateInstanceProvider) : base(aggregateInstanceProvider)
        {
            HasOne.Add(typeof(TNestedFlatModel));
        }

        public override async Task<ICommandResult> Validate(ICommandResult commandResult)
        {
            // Data cannot be Invalid
            if (InMemoryState?.Data == "Invalid")
            {
                commandResult.AddError("Data is invalid!");
            }

            return await base.Validate(commandResult);
        }
    }

    public class TNestedFlatModel : FlatModel
    {
        public Guid ParentId { get; set; }
    }

    public class TNestedAggregate : Aggregate<TNestedFlatModel>
    {
        public readonly IAggregateInstanceProvider _aggregateInstanceProvider;

        public TNestedAggregate(IAggregateInstanceProvider aggregateInstanceProvider)
        {
            _aggregateInstanceProvider = aggregateInstanceProvider;
            HasMany.Add(typeof(TDoubleNestedFlatModel));
        }

        public override async Task AttachToParent(bool toPersistedState)
        {
            var parentAggregate = _aggregateInstanceProvider.GetAggregate<TFlatModel>(InMemoryState.ParentId);
            await parentAggregate.AddNestedAggregate<TNestedAggregate,TNestedFlatModel>(this, toPersistedState);
        }
    }

    public class TDoubleNestedFlatModel : FlatModel
    {
        public Guid ParentId { get; set; }
        public string DoubleNestedValue { get; set; }
    }

    public class TDoubleNestedAggregate : Aggregate<TDoubleNestedFlatModel>
    {
        public readonly IAggregateInstanceProvider _aggregateInstanceProvider;

        public TDoubleNestedAggregate(IAggregateInstanceProvider aggregateInstanceProvider)
        {
            _aggregateInstanceProvider = aggregateInstanceProvider;
        }

        public override async Task AttachToParent(bool toPersistedState)
        {
            var parentAggregate = _aggregateInstanceProvider.GetAggregate<TNestedFlatModel>(InMemoryState.ParentId);
            await parentAggregate.AddNestedAggregate<TDoubleNestedAggregate,TDoubleNestedFlatModel>(this, toPersistedState);
        }

        public override async Task<ICommandResult> Validate(ICommandResult commandResult)
        {
            // Data cannot be Invalid
            if (InMemoryState?.DoubleNestedValue == "Invalid")
            {
                commandResult.AddError("DoubleNestedValue is invalid!");
            }

            return await base.Validate(commandResult);
        }
    }
}
