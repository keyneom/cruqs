using System;
using Xunit;
using cruqs.helpers;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;

namespace cruqs.test
{
    public class Performance
    {
        class TestDataClass
        {
            public string StringData { get; set; }
            public bool BooleanData { get; set; }
            public double DoubleData { get; set; }
            public int IntegerData { get; set; }
            public DateTime DateTimeData { get; set; }
            public Guid GuidData { get; set; }
        }

        [Fact]
        public void TestCreate()
        {
            var c = new Dictionary<string, string>()
            {
                {"StringData", "Updated!"},
                {"BooleanData", "true"},
                {"GuidData", Guid.NewGuid().ToString()},
                {"IntegerData", "-13"},
                {"DoubleData", "12.33333"},
                {"DateTimeData", "2017-10-4"}
            };
            var t = typeof(TestDataClass).Create<TestDataClass>(c);
            Console.WriteLine("Create Worked!!!");
            Console.WriteLine(t);
        }

        [Fact]
        public void UpdatePerformance()
        {
            //while(!Debugger.IsAttached) Thread.Sleep(500);
            Console.WriteLine("Update");
            var t = new TestDataClass()
            {
                StringData = "RandomString"
            };
            var u = new Dictionary<string, string>()
            {
                {"StringData", "Updated!"},
                {"BooleanData", "true"},
                {"GuidData", Guid.NewGuid().ToString()},
                {"IntegerData", "-13"},
                {"DoubleData", "12.33333"},
                {"DateTimeData", "2017-10-4"}
            };

            Time(() =>
            {
                t.StringData = u.ContainsKey("StringData") ? u["StringData"] : t.StringData;
                t.BooleanData = u.ContainsKey("BooleanData") ? Boolean.Parse(u["BooleanData"]) : t.BooleanData;
                t.GuidData = u.ContainsKey("GuidData") ? Guid.Parse(u["GuidData"]) : t.GuidData;
                t.IntegerData = u.ContainsKey("IntegerData") ? int.Parse(u["IntegerData"]) : t.IntegerData;
                t.DoubleData = u.ContainsKey("DoubleData") ? double.Parse(u["DoubleData"]) : t.DoubleData;
                t.DateTimeData = u.ContainsKey("DateTimeData") ? DateTime.Parse(u["DateTimeData"]) : t.DateTimeData;
            }, "Direct");
            Time(() =>
            {
                typeof(TestDataClass).Update(t, u);
            }, "Update");
            Time(() =>
            {
                var properties = t.GetType().GetCachedProperties();
                foreach (var update in u)
                {
                    var property = properties[update.Key];
                    var val = TypeDescriptor.GetConverter(property.PropertyType).ConvertFromString(update.Value);
                    property.PropertySetter<TestDataClass>()(t, val);
                }
            }, "OldSchool");
        }

        [Fact]
        public void GetterPerformance()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            Console.WriteLine("Get");
            var t = new TestDataClass()
            {
                StringData = "RandomString"
            };

            Time(() =>
            {
                var stringDataVal = t.StringData;
            }, "Direct");

            var p = new object[0];
            Time(() =>
            {
                var stringData = t.GetType().GetProperty("StringData");
                var getterMethod = stringData.GetGetMethod();
                var stringDataVal = getterMethod.Invoke(t, p);
            }, "ReflectionInvoke");

            Time(() =>
            {
                var stringData = t.GetType().GetProperty("StringData");
                var stringDataVal = stringData.GetValue(t, p);
            }, "ReflectionValue");

            var stringDataCached = t.GetType().GetCachedProperties();
            Time(() =>
            {
                var stringDataGetter = stringDataCached["StringData"].PropertyGetter<TestDataClass>();
                var stringDataVal = stringDataGetter(t);
            }, "Delegate");
        }

        [Fact]
        public void SetterPerformance()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            Console.WriteLine("Set");
            var t = new TestDataClass()
            {
                StringData = "RandomString"
            };

            Time(() =>
            {
                t.StringData = "1";
            }, "Direct");

            var p = new object[1] { "1" };
            Time(() =>
            {
                var stringData = t.GetType().GetCachedProperty("StringData");
                var setterMethod = stringData.GetSetMethod();
                setterMethod.Invoke(t, p);
            }, "ReflectionInvoke");


            Time(() =>
            {
                var stringData = t.GetType().GetCachedProperty("StringData");
                stringData.SetValue(t, "1");
            }, "ReflectionValue");

            var properties = t.GetType().GetCachedProperties();
            Time(() =>
            {
                var stringDataSetter = properties["StringData"].PropertySetter<TestDataClass>();
                stringDataSetter(t, "1");
            }, "Expression");
        }

        void Time(Action thingToTime, string name, int numberOfRuns = 1000000)
        {
            var start = DateTime.UtcNow.Ticks;
            for (var i = 0; i < numberOfRuns; i++)
            {
                thingToTime();
            }
            Console.WriteLine($"{((DateTime.UtcNow.Ticks - start) / 10000)} :{name}");
        }
    }
}
