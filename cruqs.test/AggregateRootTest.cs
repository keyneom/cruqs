using System;
using Xunit;
using cruqs.helpers;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Moq;
using FluentAssertions;
using System.Linq;
using System.Collections.ObjectModel;

namespace cruqs.test
{
    public class AggregateRootTest
    {
        public Mock<IDependencyContainer> _mockContainer { get; set; }
        public Mock<IFactRepository> _mockFactRepository { get; set; }
        public Mock<IWriteLocker> _mockWriteLocker { get; set; }
        public AggregateInstanceProvider _aggregateInstanceProvider { get; set; }
        public TRootAggregate _rootAggregate { get; set; }
        public TNestedAggregate _nestedAggregate { get; set; }
        public TDoubleNestedAggregate _doubleNestedAggregate { get; set; }
        public CommandMediator _commandMediator { get; set; }

        Guid _flatModelId;
        Create<TFlatModel> _createCommand;
        Update<TFlatModel> _updateCommand;
        Delete<TFlatModel> _deleteCommand;
        Create<TNestedFlatModel> _createNestedCommand;
        Create<TDoubleNestedFlatModel> _createDoubleNestedCommand;

        public AggregateRootTest()
        {
            // Setup if any
            _mockContainer = new Mock<IDependencyContainer>();
            _mockFactRepository = new Mock<IFactRepository>();
            _mockWriteLocker = new Mock<IWriteLocker>();

            _aggregateInstanceProvider = new AggregateInstanceProvider(_mockContainer.Object, _mockFactRepository.Object, _mockWriteLocker.Object);
            _rootAggregate = new TRootAggregate(_aggregateInstanceProvider);
            _nestedAggregate = new TNestedAggregate(_aggregateInstanceProvider);
            _doubleNestedAggregate = new TDoubleNestedAggregate(_aggregateInstanceProvider);
            _commandMediator = new CommandMediator(_aggregateInstanceProvider, _mockFactRepository.Object);

            #region ContainerSetup
            _mockContainer
                .Setup(c => c.GetInstance<IAggregate<TFlatModel>>())
                .Returns(_rootAggregate);
            _mockContainer
                .Setup(c => c.GetInstance(typeof(IAggregate<TFlatModel>)))
                .Returns(_rootAggregate);
            _mockContainer
                .Setup(c => c.GetInstance<IAggregateRoot<TFlatModel>>())
                .Returns(_rootAggregate);
            _mockContainer
                .Setup(c => c.GetInstance(typeof(IAggregateRoot<TFlatModel>)))
                .Returns(_rootAggregate);
            _mockContainer
                .Setup(c => c.GetInstance<IAggregate<TNestedFlatModel>>())
                .Returns(_nestedAggregate);
            _mockContainer
                .Setup(c => c.GetInstance(typeof(IAggregate<TNestedFlatModel>)))
                .Returns(_nestedAggregate);
            _mockContainer
                .Setup(c => c.GetInstance<IAggregate<TDoubleNestedFlatModel>>())
                .Returns(_doubleNestedAggregate);
            _mockContainer
                .Setup(c => c.GetInstance(typeof(IAggregate<TDoubleNestedFlatModel>)))
                .Returns(_doubleNestedAggregate);
            #endregion

            #region FactRepositorySetup
            _mockFactRepository
                .Setup(fr => fr.GetFactsByAggregateRootIds(It.IsAny<IEnumerable<Guid>>()))
                .Returns<IEnumerable<Guid>>((aggregateRootIds) =>
                {
                    var persistedFactsDict = aggregateRootIds.ToDictionary(id => id, id => new ReadOnlyCollection<IPersistedFact>(new IPersistedFact[] { }) as IReadOnlyCollection<IPersistedFact>);
                    return Task.FromResult(persistedFactsDict as IDictionary<Guid, IReadOnlyCollection<IPersistedFact>>);
                });
            #endregion

            #region WriteLockerSetup
            _mockWriteLocker
                .Setup(fr => fr.Lock(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TimeSpan>()))
                .ReturnsAsync(true);
            _mockWriteLocker
                .Setup(fr => fr.Release(It.IsAny<string>()))
                .ReturnsAsync(true);
            #endregion

            _flatModelId = Guid.NewGuid();
            _createCommand = new Create<TFlatModel>()
            {
                CommandId = Guid.NewGuid(),
                AggregateRootId = _flatModelId,
                EntityId = _flatModelId,
                Entity = new Dictionary<string, string>()
                {
                    { nameof(TFlatModel.Id), _flatModelId.ToString() },
                    { nameof(TFlatModel.Data), "Initial Value" },
                    { nameof(TFlatModel.Deleted), "false" },
                }
            };
            _updateCommand = new Update<TFlatModel>()
            {
                CommandId = Guid.NewGuid(),
                AggregateRootId = _flatModelId,
                EntityId = _flatModelId,
                Updates = new Dictionary<string, string>()
                {
                    { nameof(TFlatModel.Data),"Valid" },
                }
            };
            _deleteCommand = new Delete<TFlatModel>()
            {
                CommandId = Guid.NewGuid(),
                AggregateRootId = _flatModelId,
                EntityId = _flatModelId,
            };
            var nestedAggregateId = Guid.NewGuid();
            _createNestedCommand = new Create<TNestedFlatModel>()
            {
                CommandId = Guid.NewGuid(),
                AggregateRootId = _flatModelId,
                EntityId = nestedAggregateId,
                Entity = new Dictionary<string, string>()
                {
                    { nameof(TNestedFlatModel.Id), nestedAggregateId.ToString() },
                    { nameof(TNestedFlatModel.ParentId), _flatModelId.ToString() },
                    { nameof(TNestedFlatModel.Deleted), "false" },
                }
            };
            var doubleNestedAggregateId = Guid.NewGuid();
            _createDoubleNestedCommand = new Create<TDoubleNestedFlatModel>()
            {
                CommandId = Guid.NewGuid(),
                AggregateRootId = _flatModelId,
                EntityId = doubleNestedAggregateId,
                Entity = new Dictionary<string, string>()
                {
                    { nameof(TDoubleNestedFlatModel.Id), doubleNestedAggregateId.ToString() },
                    { nameof(TDoubleNestedFlatModel.ParentId), nestedAggregateId.ToString() },
                    { nameof(TDoubleNestedFlatModel.DoubleNestedValue), "Valid" },
                    { nameof(TDoubleNestedFlatModel.Deleted), "false" },
                }
            };
        }

        void Dispose()
        {
            // Cleanup if any
        }

        [Fact]
        public async Task CommandResultFailsIfInMemoryStateIsNull()
        {
            //while(!Debugger.IsAttached) Thread.Sleep(500);
            var commandResult = new CommandResult(Guid.Empty);
            await _rootAggregate.Validate(commandResult);
            Assert.True(commandResult.Status == ResultStatus.Failure);
        }

        [Fact]
        public async Task CreateCommandSetsInMemoryState()
        {
            // while (!Debugger.IsAttached) Thread.Sleep(500);
            var commandResults = await _commandMediator.Execute(new[] { _createCommand });
            Assert.True(commandResults.First().Value.Status == ResultStatus.Success);
            foreach (var propKv in _createCommand.Entity)
            {
                var objectVal = typeof(TFlatModel).GetProperty(propKv.Key).GetValue(_rootAggregate.InMemoryState, null);
                objectVal.ToString().ToUpperInvariant().Should().BeEquivalentTo(propKv.Value.ToUpperInvariant());
            }
        }

        [Fact]
        public async Task ShouldUndoInMemoryStateIfShouldPersistIsFalse()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            _createCommand.ShouldPersist = false;
            var commandResults = await _commandMediator.Execute(new[] { _createCommand });
            Assert.True(commandResults.First().Value.Status == ResultStatus.Success);
            Assert.Null(_rootAggregate.InMemoryState);
        }

        [Fact]
        public async Task ShouldStillProvideFactsGeneratedIfShouldPersistIsFalse()
        {
            //while(!Debugger.IsAttached) Thread.Sleep(500);
            _createCommand.ShouldPersist = false;
            var commandResults = await _commandMediator.Execute(new ICommand[] { _createCommand });
            Assert.True(commandResults.All(cr => cr.Value.FactsGenerated.Any()));
        }

        [Fact]
        public async Task ShouldReturnSuccessfulCommandResultIfShouldPersistIsFalseAndDataIsValid()
        {
            //while(!Debugger.IsAttached) Thread.Sleep(500);
            _createCommand.ShouldPersist = false;
            var commandResults = await _commandMediator.Execute(new ICommand[] { _createCommand });
            Assert.True(commandResults.All(cr => cr.Value.Status == ResultStatus.Success));
        }

        [Fact]
        public async Task ShouldReturnFailedCommandResultIfShouldPersistIsFalseAndDataIsInvalid()
        {
            //while(!Debugger.IsAttached) Thread.Sleep(500);
            _createCommand.ShouldPersist = false;
            _createCommand.Entity[nameof(TFlatModel.Data)] = "Invalid";
            var commandResults = await _commandMediator.Execute(new ICommand[] { _createCommand });
            Assert.True(commandResults.All(cr => cr.Value.Status == ResultStatus.Failure));
        }

        [Fact]
        public async Task CommandResultSucceedsOnUpdate()
        {
            //while(!Debugger.IsAttached) Thread.Sleep(500);
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _updateCommand,
            });
            Assert.True(commandResults.All(cr => cr.Value.Status == ResultStatus.Success));
        }

        [Fact]
        public async Task UpdateCommandUpdatesInMemoryState()
        {
            //while(!Debugger.IsAttached) Thread.Sleep(500);
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _updateCommand,
            });
            Assert.Equal(_rootAggregate.InMemoryState.Data, _updateCommand.Updates[nameof(TFlatModel.Data)]);
        }

        [Fact]
        public async Task CommandResultSucceedsOnDelete()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _deleteCommand,
            });
            Assert.True(commandResults.All(cr => cr.Value.Status == ResultStatus.Success));
        }

        [Fact]
        public async Task DeleteCommandMarksInMemoryStateAsDeleted()
        {
            //while(!Debugger.IsAttached) Thread.Sleep(500);
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _deleteCommand,
            });
            Assert.True(_rootAggregate.InMemoryState.Deleted);
        }

        [Fact]
        public async Task CommandResultFailsIfDataIsInvalid()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            _createCommand.Entity[nameof(TFlatModel.Data)] = "Invalid";
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
            });
            Assert.True(commandResults.All(cr => cr.Value.Status == ResultStatus.Failure));
        }

        [Fact]
        public async Task NestedAggregateIsAddedToParentWhenCreated()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _createNestedCommand,
            });
            Assert.True(_rootAggregate.HasOneNestedAggregates.ContainsValue(_nestedAggregate));
        }

        [Fact]
        public async Task TNestedFlatModelCannotHaveTheSameIdAsItsParent()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            _createNestedCommand.EntityId = _flatModelId;
            _createNestedCommand.Entity[nameof(TNestedFlatModel.Id)] = _flatModelId.ToString();
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _createNestedCommand,
            });
            Assert.True(commandResults[_createNestedCommand.CommandId].Status == ResultStatus.Failure);
        }

        [Fact]
        public async Task FlatModelCannotHaveEmptyGuidForId()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            _createNestedCommand.EntityId = Guid.Empty;
            _createNestedCommand.Entity[nameof(TNestedFlatModel.Id)] = Guid.Empty.ToString();
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _createNestedCommand,
            });
            Assert.True(commandResults[_createNestedCommand.CommandId].Status == ResultStatus.Failure);
        }

        [Fact]
        public async Task NestedAggregateIsRemovedFromParentWhenNestedCreateFails()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            _createNestedCommand.EntityId = Guid.Empty;
            _createNestedCommand.Entity[nameof(TNestedFlatModel.Id)] = Guid.Empty.ToString();
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _createNestedCommand,
            });
            Assert.False(_rootAggregate.HasOneNestedAggregates.ContainsValue(_nestedAggregate));
        }

        
        [Fact]
        public async Task DoubleNestedAggregateIsAddedCorrectlyWhenCreated()
        {
            // while(!Debugger.IsAttached) Thread.Sleep(500);
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _createNestedCommand,
                _createDoubleNestedCommand,
            });
            Assert.True(_nestedAggregate.HasManyNestedAggregates[typeof(TDoubleNestedFlatModel)].ContainsValue(_doubleNestedAggregate));
        }

        
        [Fact]
        public async Task CommandResultFailsIfDoubleNestedValueIsInvalid()
        {
            while(!Debugger.IsAttached) Thread.Sleep(500);
            _createDoubleNestedCommand.Entity[nameof(TDoubleNestedFlatModel.DoubleNestedValue)] = "Invalid";
            var commandResults = await _commandMediator.Execute(new ICommand[]
            {
                _createCommand,
                _createNestedCommand,
                _createDoubleNestedCommand,
            });
            Assert.True(commandResults.Any(cr => cr.Value.Status == ResultStatus.Failure && cr.Value.Errors.Any(e => e.Contains("DoubleNestedValue is invalid"))));
        }
    }
}
